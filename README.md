## Terraform

ECS stack: This is a high-level setup of AWS infrastructure for a dockerized application running on ECS with Fargate launch configuration using terraform.

## Resources

With this script the following resources are created:

- VPC
- One public and one private subnet per AZ
- Routing tables for the subnets
- Internet Gateway for public subnets
- NAT gateways with attached Elastic IPs for the private subnet
- An ALB + target group with listeners for port 80 and 443
- An ECR for the docker images
- An ECS cluster with a service (incl. auto scaling policies for CPU and memory usage)
  and task definition to run docker containers from the ECR (incl. IAM execution role)

![example](https://cactustesting.s3.ap-south-1.amazonaws.com/ECSStack.png "Illustration of AWS Infrastructure")
(Source: https://aws.amazon.com/de/blogs/compute/task-networking-in-aws-fargate/)

## Bitbucket Pipeline
Executing this script via CI/CD bitbucketpipeline.yml file

Image used to run terraform

- image: hashicorp/terraform:full

Following is the script which will be executed as part of pipeline:

- terraform init
- terraform validate
- terraform plan
- terraform apply -input=false -auto-approve

Rollback: By replacing 'apply' with 'destroy' in the .yml file the infrastructure just created can be reverted/destroyed

- eg: terraform destroy -input=false -auto-approve